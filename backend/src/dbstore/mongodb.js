import mongodb from 'mongodb';
import {CONNECTION_URL} from '../constants/constants';

class Connection {
    static async connect(dbname){
        if(this.db)
            return Promise.resolve(this.db);
  
        return await mongodb.MongoClient.connect(CONNECTION_URL,{useNewUrlParser: true})
                    .then(async client =>{ this.db = await client.db(dbname)});
    }
}

Connection.db = null;


module.exports = { Connection }






