import chai from 'chai';
import chaiHttp from 'chai-http';

import app from '../server';
import connectdb from '../utils/initializedb';

import {MOCK_POST, MOCK_PUT,MOCK_PATCH, MOCK_DELETE_ID} from '../constants/constants';

chai.use(chaiHttp);
chai.should();

describe("Templates CRUD", () => {
    
    // let db = null;
    beforeEach( async() => {
        //for test db
        process.env.NODE_ENV = 'test';
        await connectdb();
    });

    describe("/GET templates", ()=>{

        it("should get empty result", (done) => {
            chai.request(app)
                .get('/template')
                .end((err,res)=> {
                    if(err)
                        console.log(err);
                    else{
                        res.should.have.status(204);
                        res.body.should.be.a('object');
                        done();
                    }   
                });
        });

    });


    describe("/POST templates", ()=>{

        it("should post mock data", (done) => {
            chai.request(app)
                .post('/template')
                .send(MOCK_POST)
                .end((err,res) =>{
                    if(err)
                        console.log(err);
                    else
                        res.should.have.status(201);
                        done();
                })
        });

        it("should return 401 when no body passed", (done) => {
            chai.request(app)
                .post('/template')
                .send()
                .end((err,res) =>{
                    if(err)
                        console.log(err);
                    else
                        res.should.have.status(401);
                        done();
                })
        });

    });

    describe("/GET templates", ()=>{

        it("should get all templates", (done) => {
            chai.request(app)
                .get('/template')
                .end((err,res)=> {
                    if(err)
                        console.log(err);
                    else{
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        done();
                    }   
                });
        });
        
    });

    describe("/PUT templates", ()=>{

        it("should update mock data", (done) => {
            chai.request(app)
                .put('/template')
                .send(MOCK_PUT)
                .end((err,res) =>{
                    if(err)
                        console.log(err);
                    else
                        res.should.have.status(200);
                        done();
                })
        });

        it("should return 401 when no object passed", (done) => {
            chai.request(app)
                .put('/template')
                .send()
                .end((err,res) =>{
                    if(err)
                        console.log(err);
                    else
                        res.should.have.status(401);
                        done();
                })
        });
    });

    describe("/PATCH templates", ()=>{

        it("should patch mock data", (done) => {
            chai.request(app)
                .put('/template')
                .send(MOCK_PATCH)
                .end((err,res) =>{
                    if(err)
                        console.log(err);
                    else
                        res.should.have.status(200);
                        done();
                })
        });

        it("should return 401 when no object passed", (done) => {
            chai.request(app)
                .put('/template')
                .send()
                .end((err,res) =>{
                    if(err)
                        console.log(err);
                    else
                        res.should.have.status(401);
                        done();
                })
        });

    });


    describe("/DELETE templates", ()=>{

        it("should delete mock id", (done) => {
            chai.request(app)
                .delete('/template/'+MOCK_DELETE_ID)
                .end((err,res) =>{
                    if(err)
                        console.log(err);

                    res.should.have.status(200);
                    done();
                })
        });


        it("should delete data", (done) => {
            chai.request(app)
                .delete('/template')
                .end((err,res) =>{
                    if(err)
                        console.log(err);

                    res.should.have.status(200);
                    done();
                })
        })

    });


})