import express from 'express';
import {PORT, DBNAME} from './constants/constants';
import router from './router/router';
import connectdb from './utils/initializedb';

const app = express();

//Parse request into body
app.use(express.json());

//Connect to mongo
connectdb();

//Set Headers
app.use( (req,res,next) => {
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

//Routes
app.use('/',router);

app.listen(PORT, () => {
    console.log("server listening on port 3000");
});

export default app;