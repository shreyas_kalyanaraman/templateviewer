export const PORT = 3002;

export const CONNECTION_URL = 'mongodb://localhost:27017';

export const DBNAME = 'templates';
export const TEST_DBNAME = 'test';

export const MOCK_POST = [
    {
        "title": "Business Site Template - 7111",
        "cost": "45.00",
        "id": "7111",
        "description": "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
        "thumbnail": "7111-m.jpg",
        "image": "7111-b.jpg"
    },
    {
        "title": "Business Site Template - 7112",
        "cost": "55.00",
        "id": "7112",
        "description": "Laoreet eu amet soluta error a nulla, sed maecenas est risus augue turpis varius, torquent fermentum diam in augue.",
        "thumbnail": "7112-m.jpg",
        "image": "7112-b.jpg"
    },
];

export const MOCK_PUT = [
    {
        "title": "Business Site Template - 7111",
        "cost": "45.00",
        "id": "7111",
        "description": "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
        "thumbnail": "7111-b.jpg",
        "image": "7111-b.jpg"
    },
    {
        "title": "Business Site Template - 7112",
        "cost": "55.00",
        "id": "7112",
        "_id":"7112-abc",
        "description": "Laoreet eu amet soluta error a nulla, sed maecenas est risus augue turpis varius, torquent fermentum diam in augue.",
        "thumbnail": "7112-m.jpg",
        "image": "7112-b.jpg"
    },
];

export const MOCK_PATCH = [{
    "title": "Business Site Template - 7113",
    "cost": "45.00",
    "id": "7113",
    "description": "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
    "thumbnail": "7111-m.jpg",
    "image": "7111-b.jpg"
}];

export const MOCK_DELETE_ID = "7113";