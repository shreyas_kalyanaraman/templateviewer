import {Connection} from '../dbstore/mongodb';
import {success, error} from '../utils/response';

const get = async () => {

    const db = Connection.db;

    if(db === 'undefined' || !db)
        return error("Server Timeout. Try after sometime",401);
    
    try{
    const result = await db.collection('template').find().toArray();

    if(result === 'undefined' || result.length === 0)
        return success("No Content",204);

    return success(result,200);
    }catch(err){
        console.log(err);
        return error("Server timeout",400);
    }
}


const create = async (body) => {

    const db = Connection.db;

    if(db === 'undefined')
        return error("Server Timeout. Try after some time",401);

    try{
        const result = await db.collection('template').insertMany(body);

        return success("Inserted successfully",201);
    }catch(err){
        console.log(err);
        return error("Server timeout", 400);
    }
}

/*
    Steps for put (update)
    1. Fetch existing templates from db (say DB)
    2. Compare id of values DB and current array of values
    3. If values differ update, else skip the object and move on to next
    4. If id does not exist, push new record
*/

const update = async (body) => {
    const db = Connection.db;

    if(db === 'undefined')
        return error("Server Timeout. Try after some time",401);

    const response = await get();

    if(response.status !== 200){
        return error(response.body,response.status);
    }

    //Step 1.
    const dataInDb = response.body;
    let isModified = false;

    for(let bodyKey in body){
        
        let isKeyPresent = false;

        for(let dbKey in dataInDb){

            //Step 2.
            if(dataInDb[dbKey].id === body[bodyKey].id ){


                for(let keyvalue in body[bodyKey]){

                    //Step 3.
                    if(dataInDb[dbKey][keyvalue] !== body[bodyKey][keyvalue]){
                        try{
                            delete body[bodyKey]._id;
                            const response = await db.collection('template').updateOne({_id:dataInDb[dbKey]._id}, {$set : body[bodyKey]});
                            isModified = true;
                            
                        }catch(err){
                            console.log(err);
                            return error("Server timeout. Please try after sometime",400);
                        }
                        break;
                    }

                }

                isKeyPresent = true;
                break;
            }
        }

        //Step 4.
        if(!isKeyPresent){
            try{
                const response = await db.collection('template').insertOne(body[bodyKey]);
            }catch(err){
                console.log(err);
                return error("Server timeout",400);
            }
            isModified = true;
        }

    }   

    if(isModified){
        return success("Value updated",200);
    }

    return success("No values updated",200);

}


/*
    Steps for patch
    1. Fetch existing templates from db (say DB)
    2. Compare id of values DB and current object
    3. If values differ update, else skip the object and move on to next
    4. If id does not exist, push new record
*/
const patch = async (body) => {
    const db = Connection.db;

    if(db === 'undefined')
        return error("Server Timeout. Try after some time",401);

    const response = await get();

    if(response.status !== 200){
        return error(response.body,response.status);
    }

    const dataInDb = response.body;
    let isModified = false;

    let isKeyPresent = false;

    for(let dbKey in dataInDb){
        if(dataInDb[dbKey].id === body.id ){
            for(let keyvalue in body){
                if(dataInDb[dbKey][keyvalue] !== body[keyvalue]){
                    try{
                        const response = await db.collection('template').updateOne({_id:dataInDb[dbKey]._id}, {$set : body});
                        if(response[0].nModified === 1)
                            isModified = true;
                        
                    }catch(err){
                        console.log(err);
                        return error("Server timeout",400);
                    }
                    break;
                }
            }
            isKeyPresent = true;
            break;
        }
    }
    if(!isKeyPresent){
        try{
            const response = await db.collection('template').insertOne(body);
        }catch(err){
            console.log(err);
            return error("Server timeout",400);
        }
        isModified = true;
    }

    if(isModified){
        return success("Value updated",200);
    }

    return success("No values updated",200);


}

// Delete based on id
const deleteId = async (id) => {

    const db = Connection.db;

    if(db === 'undefined')
        return error("Server Timeout. Try after some time",401);

    try{
        const response = await db.collection('template').deleteOne({id:id});

        return success("Deleted successfully",200);
    }catch(err){
        console.log(err);
        return error("Server timeout",400);
    }
}

//Delete all templates from db
const deleteall = async (id) => {

    const db = Connection.db;

    if(db === 'undefined')
        return error("Server Timeout. Try after some time",401);

    try{
        const response = await db.collection('template').deleteMany();

        return success("Deleted successfully",200);
    }catch(err){
        console.log(err);
        return error("Server timeout",400);
    }
}

module.exports = {
    get,
    create,
    update,
    patch,
    deleteId,
    deleteall
}