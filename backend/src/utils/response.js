export const success = (message,status) => {
    return {
        success: true,
        body : message,
        status
    };
}

export const error = (message,status) => {
    return {
        error: true,
        body: message,
        status
    };
}