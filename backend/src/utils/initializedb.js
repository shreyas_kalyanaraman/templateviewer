import {Connection} from '../dbstore/mongodb';
import {DBNAME, TEST_DBNAME} from '../constants/constants';

const connectdb = async () => {
    
    let dbname = DBNAME;
    if(process.env.NODE_ENV === 'test')
        dbname = TEST_DBNAME;

    await Connection.connect(dbname)
            .then(res => {
                return res;
            }).catch(err => {
                console.log(err);
                return res;
            });
}  

export default connectdb;
