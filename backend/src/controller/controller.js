import services from '../service/service';
import { isArray } from 'util';

const getController = async (req,res) => {
    
    const result = await services.get();

    return res.status(result.status).send(result);
}

const postController = async (req,res) => {

    const body = req.body;

    if(body === 'undefined' || body.length === 0 || Object.entries(body).length === 0)
        return res.status(401).send({message: "Body is required"});

    const result = await services.create(body);

    return res.status(result.status).send(result);
}

const putController = async (req,res) => {

    const body = req.body;

    if(body === 'undefined' || body.length === 0 || Object.entries(body).length === 0)
        return res.status(401).send({message: "Body is required"});

    const result = await services.update(body);

    return res.status(result.status).send(result);

}

const patchController = async (req,res) => {
    const body = req.body;

    if(body === 'undefined' || body.length === 0 || Object.entries(body).length === 0)
        return res.status(401).send({message: "Body is required"});

    const result = await services.patch(body);

    return res.status(result.status).send(result);

}

const deleteController = async (req,res) => {
    try{
        const id = parseInt(req.params.id);

        if(!id || id === 0)
            return res.status(401).send({error:true,message:"id is mandatory"});

        const result = await services.deleteId(id);

        return res.status(result.status).send(result);
    }catch(err){
        return res.status(401).send({error:true,message:"id must be integer"});
    }
}

const deleteAllController = async (req,res) => {

    const result = await services.deleteall();

    return res.status(result.status).send(result);
}


module.exports = {
    getController,
    postController,
    putController,
    patchController,
    deleteController,
    deleteAllController
}