import express from 'express';
import controllers from '../controller/controller';


const router = express.Router();

router.get('/template',controllers.getController);

router.post('/template',controllers.postController);

router.put('/template',controllers.putController);

router.patch('/template',controllers.patchController);

router.delete('/template/:id',controllers.deleteController);

router.delete('/template',controllers.deleteAllController);

module.exports = router;