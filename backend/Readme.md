# Backend

A Rest API back end server built using Node.js and Express.js. This api serves as backend server for the frontend react app part.  

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
1. Code editing softwares necessary to edit the contents and images. The following softwares are just few of the examples, but one can use any software they are comfortable with
- Visual studio code
- Atom
- Eclipse
- Intellij 
- .... many more
2. Latest version of Node.js. In case you don't have it please install from [here](https://nodejs.org/en/). Recommended: download LTS version.  

### Installing

#### Step 1: Install the dependencies

```
npm install
```

#### Step 2: Start the application

```
npm start
```

Thats it! Simple and easy.

#### Whats inside?
A quick look at directories inside the project
```
.
|- backend
  |- node_modules
  |- src
    |- constants
    |- controller
    |- dbstore
    |- router
    |- service
    |- utils
    |- server.js
    |- .babelrc
    |- package-lock.json
    |- package.json
```



