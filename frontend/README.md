# TemplateViewer

Frontend webapp part built using React.js. This app servers as front-end for the backend thereby displaying the image viewer UI. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
1. Code editing softwares necessary to edit the contents and images. The following softwares are just few of the examples, but one can use any software they are comfortable with
- Visual studio code
- Atom
- Eclipse
- Intellij 
- .... many more
2. Latest version of Node.js. In case you don't have it please install from [here](https://nodejs.org/en/). Recommended: download LTS version.  

### Installing

#### Step 1: Install the dependencies

```
npm install

```

#### Step 2: Start the application

```
npm start
```

Thats it! Simple and easy.

#### Step 3: To test the application

```
npm run test
```

#### Whats inside?
A quick look at directories inside the project

```
.
|- frontend
  |- config
  |- node_modules
  |- public
  |- scripts
  |- src
    |- assets
    |- components
    |- constants
    |- container
    |- hoc
    |- App.css
    |- App.js
    |- App.test.js
    |- axios-data.js
    |- index.css
    |- index.js
    |- serviceWorker.js
  |- .env
  |- .gitignore
  |- package-lock.json
  |- package.json
  |- Readme.md
```



