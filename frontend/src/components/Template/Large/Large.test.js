import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {MOCK_OBJECT} from 'constants/constants';
import Large from './Large';

configure({
    adapter: new Adapter(),
    disableLifecycleMethods:true,
})


describe('<Large/>', () => {

    it('should display image when props passed', () => {
        const wrapper = shallow(<Large data={MOCK_OBJECT}/>);
        expect(wrapper.find('img')).toHaveLength(1);
    });

    it('should display description when props passed', () => {
        const wrapper = shallow(<Large data={MOCK_OBJECT}/>);
        expect(wrapper.find('[test-data="image-desc"]')).toHaveLength(1);
    });
})