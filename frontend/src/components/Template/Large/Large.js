import React from 'react';

import classes from './Large.css';

const large = (props) => {

    let image = (<img id="large-image" src="" alt={props.data.title}/>);
    let imageClass = [classes.Download]
    if(props.isDownload){
        image = (<img id="large-image" src={require('assets/images/Large/'+props.data.image)} alt={props.data.title}/>);
    }else{
        imageClass = [classes.Download,classes.Disabled];
    }

    return (
        <div className={classes.Large}>
            {image}
            <div className={classes.Details} test-data="image-desc">
                <strong>{props.data.title}</strong>
                <p>Cost: {props.data.cost}</p>
                <p>Description: {props.data.description}</p>
                <p>Thumbnail File Name: {props.data.thumbnail}</p>
                <p>Image File Name: {props.data.image}</p>
                <p><button className={imageClass.join(' ')} onClick={props.download}>Download PDF</button></p>
            </div>
        </div>
    );
};

export default large;