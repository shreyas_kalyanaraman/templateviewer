import React from 'react';

import Aux from 'hoc/Auxillary';
import classes from './Controls.css';

import Previous from 'assets/images/previous.png';
import Next from 'assets/images/next.png';

const controls = (props) => {

    let nextStyle = [classes.Next];
    let prevStyle = [classes.Previous];

    if(!props.showNext)
        nextStyle = [classes.Next, classes.Disabled];

    if(!props.showPrevious)
        prevStyle = [classes.Previous, classes.Disabled];

    return (
        <Aux>
            <img className={prevStyle.join(' ')} src={Previous} onClick={props.previous} alt="Previous"/>
            <img className={nextStyle.join(' ')} src={Next} onClick={props.next} alt="Next"/>
        </Aux>
    );
};

export default controls;