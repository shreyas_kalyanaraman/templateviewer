import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {MOCK_DATA} from 'constants/constants';

import Thumbnails from './Thumbnails';
import Thumbnail from './Thumbnail/Thumbnail';

configure({
    adapter: new Adapter(),
    disableLifecycleMethods:true,
})


describe('<Thumbnails/>', () => {

    let wrapper = null;
    beforeEach(() => {
        wrapper = shallow(<Thumbnails/>);
    })

    it('should render thumbnails component without crashing',() => {
        const div = document.createElement('div');
        ReactDOM.render(<Thumbnails/>,div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('should render no images found, if no props passed', () => {
        expect(wrapper.find('[test-data="No-image"]')).toHaveLength(1);
    })

    it('should render <Thumbnail> two times when two templates passed', () => {
        wrapper = shallow(<Thumbnails templates={MOCK_DATA.message}/>);
        expect(wrapper.find(Thumbnail)).toHaveLength(2);
    })


})