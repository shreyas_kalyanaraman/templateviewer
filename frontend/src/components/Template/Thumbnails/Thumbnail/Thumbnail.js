import React from 'react';

// import {THUMBNAIL_URL} from 'constants/constants';

import classes from './Thumbnail.css';

const thumbnail = (props) => { 
    
    let image = (<img id="large-image" src="" alt={props.data.title}/>);
    try{
        image = (<img id="large-image" src={require('assets/images/Large/'+props.data.image)} alt="Template-1"/>);
    }catch(err){
        
    }

    return (
        <span className={classes.Thumbnail} key={props.data._id}>
            <button className={props.data.isSelected ? classes.active : null } onClick={props.selected}>
                {image}
            </button>
        </span>
    );
}

export default thumbnail;