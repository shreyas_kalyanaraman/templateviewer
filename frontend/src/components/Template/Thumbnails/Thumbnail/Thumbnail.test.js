import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {MOCK_OBJECT} from '../../../../constants/constants';
import Thumbnail from './Thumbnail';

configure({
    adapter: new Adapter(),
    disableLifecycleMethods:true,
})


describe('<Thumbnail/>', () => {

    it('should display image when props passed', () => {
        const wrapper = shallow(<Thumbnail data={MOCK_OBJECT}/>);
        expect(wrapper.find('img')).toHaveLength(1);
    });
})