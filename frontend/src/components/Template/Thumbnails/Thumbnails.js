import React from 'react';

import Thumbnail from './Thumbnail/Thumbnail';

import classes from './Thumbnails.css';

const thumbnails = (props) => {

    let thumbnail_display = (<h4 test-data="No-image">No templates to display</h4>);

    if(props.templates){

        thumbnail_display = props.templates.map( obj => {
            return (<Thumbnail key={obj._id} data = {obj} selected = {() => props.select(obj._id)}/>); 
        });

    }

    return (
        <div className={classes.Thumbnails}>
            <div className={classes.group}>
                {thumbnail_display}
            </div>
        </div>
    );
};

export default thumbnails;