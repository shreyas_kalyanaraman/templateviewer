import React,{Component} from 'react';

import jspdf from 'jspdf';
import html2canvas from 'html2canvas';

import Large from 'components/Template/Large/Large';
import Controls from 'components/Template/Controls/Controls';
import Thumbnail  from 'components/Template/Thumbnails/Thumbnails';

import {TEMPLATE_SIZE} from 'constants/constants';

import axios from 'axios-data';

import classes from './TemplateViewer.css';

export class TemplateViewer extends Component{

    _isMounted = false;

    state = {
        templates : null,
        currentTemplates : null,
        large: null,
        previous:false,
        next:false,
        currIndex:-1,
        pageNo:1,
        numofPages:0,
        download:true,
    }

    componentDidMount = async () => {

        this._isMounted = true;

        let body = [];

        await axios.get('/template')
          .then( res=>{
              if(res.status === 200){
                  if(res.data)
                    body = res.data.body;
              }
          }).catch(err=>{
              console.log(err);
          });

        if(body.length === 0)
            return;

        const template = body.map( obj => {
            return {
                        ...obj,
                        isSelected:false,
                    };
        });

        //First Image
        const largeImage = body[0];

        const currentTemplate = body.slice(0,TEMPLATE_SIZE);
        currentTemplate[0].isSelected = true;

        const numofPages = body.length / TEMPLATE_SIZE;


        if(this._isMounted)
            this.setState({
                templates:template, 
                large:largeImage, 
                currIndex:0,
                next:true,
                currentTemplates:currentTemplate,
                numofPages:numofPages});
    }

    componentWillUnmount = () => {
        this._isMounted = false;
    }

    enlageImageHandler = (id) => {
        
        this._isMounted = true;

        if(this.state.templates === null)
            return;

        const templates = [...this.state.templates];

        let template = null;
        let currentIndex = -1;
        for(let i=0; i<templates.length;i++){
            if(templates[i]._id === id){
                template = {...this.state.templates[i]};
                currentIndex = i;
            }

            templates[i].isSelected = false;
        }

        //check image exists to disable download button
        let isExists = true;
        try{
            const image = (require('assets/images/Large/'+template.image));
        }catch(exception){
            isExists = false;
        }


        let currentTemplate = [...this.state.currentTemplates]
        for(let keys in currentTemplate){
            if(currentTemplate[keys]._id === template._id){
                currentTemplate[keys].isSelected = true;
                continue;
            }
            currentTemplate[keys].isSelected = false;
        }
        
        if(this._isMounted)
            this.setState({
                large:template,
                currentTemplates: currentTemplate,
                currIndex: currentIndex,
                next: currentIndex!== (templates.length - 1),
                previous: (currentIndex !== 0),
                download:isExists,
            });
    }

    nextImageHandler = () => {

        this._isMounted = true;

        if(this.state.templates === null)
            return;

        const templates = [...this.state.templates];

        const noOfPages = this.state.numofPages;

        if(this.state.pageNo < noOfPages){

            let currentTemplate = [...this.state.currentTemplates];
            let currentPageNo = this.state.pageNo;

            let newIndex = TEMPLATE_SIZE*currentPageNo;
            currentTemplate = templates.slice(newIndex,newIndex+TEMPLATE_SIZE);
            currentPageNo +=1;

            if(this._isMounted)
                this.setState({ 
                    currentTemplates:currentTemplate,
                    pageNo:currentPageNo,
                    previous:true, 
                    next: (currentPageNo < noOfPages),   
                });
        }
    }

    previousImageHandler = () => {

        this._isMounted = true;

        if(this.state.templates === null)
            return;

        const templates = [...this.state.templates];

        if(this.state.pageNo > 1){
            

            let currentTemplate = [...this.state.currentTemplates];
            let currentPageNo = this.state.pageNo;

            currentPageNo -= 1;

            let newIndex = TEMPLATE_SIZE*currentPageNo - TEMPLATE_SIZE;
            currentTemplate = templates.slice(newIndex,TEMPLATE_SIZE+newIndex);

            if(this._isMounted)
                this.setState({
                    currentTemplates:currentTemplate,
                    pageNo:currentPageNo,
                    next:true,
                    previous: (currentPageNo > 1),
                });

        }

    }

    downloadImageHandler = (image) => {

        if(!this.state.download)
            return;

        const largeImage = document.getElementById("large-image");

        html2canvas(largeImage)
            .then(canvas => {
                const imgData = canvas.toDataURL('image/png');
                const jsPdf = new jspdf("p", "mm", "a4");

                //full screen
                // var width = jsPdf.internal.pageSize.getWidth();
                // var height = jsPdf.internal.pageSize.getHeight();

                // jsPdf.addImage(imgData,'PNG',0,0,width,height);

                jsPdf.addImage(imgData,0,0);
                jsPdf.save(image+".pdf");
            });

    }

    render(){
        let expandedImage = (<span test-data="No-image"><h4>No Image to display</h4></span>)
        if(this.state.large)
            expandedImage = (<Large data={this.state.large} isDownload={this.state.download} download={() => this.downloadImageHandler(this.state.large.title)}/>);

        return (
            <div className={classes.TemplateViewer}>
                <header>Template Viewer</header>
                {expandedImage}
                <Controls 
                    next={this.nextImageHandler} 
                    previous={this.previousImageHandler} 
                    showNext={this.state.next}
                    showPrevious={this.state.previous}/>
                <Thumbnail templates = {this.state.currentTemplates} select = {this.enlageImageHandler}/>
                <footer>Developed by Shreyas_K</footer>
            </div>
        );
    }
}

export default TemplateViewer;