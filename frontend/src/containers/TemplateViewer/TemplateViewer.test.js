import React from 'react';
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';


import {TemplateViewer} from './TemplateViewer';
import Large from 'components/Template/Large/Large';
import {MOCK_DATA, MOCK_OBJECT} from 'constants/constants';


configure({
    adapter:new Adapter(),
    disableLifecycleMethods:true,
});

describe("<TemplateViewer/>",() =>{

    let wrapper = null;
    beforeEach(() => {
        wrapper = shallow(<TemplateViewer/>);
    })

    const setWrapperState = () => {
        wrapper.setState({
            templates:MOCK_DATA.message,
            currentTemplates:MOCK_DATA.message,
        });

    }

    it('should not display large image when no state set', () => {
        expect(wrapper.find('[test-data="No-image"]')).toHaveLength(1);
    }); 
    
    it('previous button and next button should be disabled', () => {
        expect(wrapper.state('previous')).toBe(false);
    });

    test('previous button should be enabled',() => {
        const instance = wrapper.instance();
        expect(wrapper.state('previous')).toBe(false);

        setWrapperState();

        expect(wrapper.state('templates')).not.toBe(null);

        instance.nextImageHandler();
        expect(wrapper.state('previous')).toBe(true);
    });

    it('next button should be disabled after reaching at the end', () => {
        const instance = wrapper.instance();
        setWrapperState();

        instance.nextImageHandler();
        instance.nextImageHandler();

        expect(wrapper.state('next')).toBe(false);

    });

    it('previous button should be disabled after reaching first image', () => {
        const instance = wrapper.instance();
        setWrapperState();

        //set first Image
        instance.nextImageHandler();

        //call next image
        instance.nextImageHandler();
        
        //Go back to previous image
        instance.previousImageHandler();

        expect(wrapper.state('previous')).toBe(false);
    });

    it('should display large image when state set', ()=> {
        wrapper.setState({large:MOCK_OBJECT});
        expect(wrapper.find(Large)).toHaveLength(1);
    });

});