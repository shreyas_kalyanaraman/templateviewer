import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {TemplateViewer} from 'containers/TemplateViewer/TemplateViewer';

jest.mock('axios');

configure({
  adapter:new Adapter(),
  disableLifecycleMethods:true,
});

describe('<App/>', () => {

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should render <TemplateViewer/>', () => {
      const wrapper = shallow(<App/>);
      expect(wrapper.find(TemplateViewer)).toHaveLength(1);
  })

});