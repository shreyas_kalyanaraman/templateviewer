export const API_URL = 'http://localhost:3002'

export const LARGE_URL = "assets/images/Large/";
export const THUMBNAIL_URL = "assets/images/Thumbnails/"

export const TEMPLATE_SIZE = 4;

export const MOCK_DATA =
    {
        message : [
            {
                "title": "Business Site Template - 7111",
                "cost": "45.00",
                "id": "7111",
                "_id":"7111-abc",
                "description": "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
                "thumbnail": "7111-m.jpg",
                "image": "7111-b.jpg"
            },
            {
                "title": "Business Site Template - 7112",
                "cost": "55.00",
                "id": "7112",
                "_id":"7112-abc",
                "description": "Laoreet eu amet soluta error a nulla, sed maecenas est risus augue turpis varius, torquent fermentum diam in augue.",
                "thumbnail": "7112-m.jpg",
                "image": "7112-b.jpg"
            },
        ],
    }

export const MOCK_OBJECT = {
    "title": "Business Site Template - 7111",
    "cost": "45.00",
    "id": "7111",
    "_id":"7111-abc",
    "description": "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
    "thumbnail": "7111-m.jpg",
    "image": "7111-b.jpg"
}