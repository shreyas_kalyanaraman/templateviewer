import MOCK_DATA from 'constants/constants'


const axios = {
  get: () => {
    return Promise.resolve({
      MOCK_DATA,
      status:200
    });
  },
  create:() => axios,

};

export default axios;