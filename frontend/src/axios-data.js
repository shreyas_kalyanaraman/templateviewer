import axios from 'axios';
import {API_URL} from '../src/constants/constants';

const instance = axios.create({
    baseURL: API_URL,
});

export default instance;