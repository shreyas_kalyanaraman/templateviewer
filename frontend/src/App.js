import React from 'react';
import TemplateViewer from './containers/TemplateViewer/TemplateViewer';

import classes from './App.css';

function App() {
  return (
    <div className={classes.App}>
        <TemplateViewer/>
    </div>
  );
}

export default App;
