# TemplateViewer

A image viewer webapp built using MERN (Mongo, express, react, node) stack. Can be useful for surfing through list of various images, design options, website template designs, logo designs etc.  

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
1. Code editing softwares necessary to edit the contents and images. The following softwares are just few of the examples, but one can use any software they are comfortable with
- Visual studio code
- Atom
- Eclipse
- Intellij 
- .... many more
2. Latest version of Node.js. In case you don't have it please install from [here](https://nodejs.org/en/). Recommended: download LTS version.  
3. Latest version of Mongodb. In case you dont' have it, please install from [here](https://www.mongodb.com/download-center/community). For the case of this project, we will be using Local MongoDB server. Therefore, please download and **install the mongodb community server**.
4. Lastest version of Postman or any equivalent API/Development tools.

### Installing

It can been seen from project structure that there are two more folders. Frontend and backend. Frontend serves the react app while backend is used for starting the server.

Don't worry, there's a good news!. You will have to **run just one command** to run this app. 

But before that you need to install some dependencies. 

#### Step 1: Install the dependencies

```
1. cd templateviewer
2. npm install

3. cd frontend
4. npm install

5. cd..
6. cd backend
7. npm install

```

#### Step 2: Start the entire application

Make sure you are in root directory i.e **templateviewer** directory

```
npm run dev
```

Thats it! Simple and easy. View the app in your browser at **http://localhost:3000**

#### Step 3: Test the entire application

```
npm run test
```

### Running the application

If you run the app directly by running ``` npm run dev ```, the app says there's no image to display. Thats because there is no data. Let's load the data first.

1. Start Mongodb server (At this point this app supports local mongodb server)
2. Start postman or equivalent API developer tool. Make a post request with required JSON data to URL **https://localhost:3000/template** as per the image below

![Post Request](post-request.png)

View the app in your browser at **http://localhost:3000**

3. This app support PUT, PATCH and DELETE request as well. 

#### Whats inside?
A quick look at directories inside the project
```
.
|- backend
|- frontend
|- node_modules
|- .gitignore
|- package-lock.json
|- package.json
|- Readme.md
```



